/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algGeom.lib2D;

import java.awt.Color;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.media.opengl.GL;

/**
 *
 * @author Javier Mart�nez Cavanillas
 */
public class CircleGrid extends Draw
{
    List<Circle> circles;
    Vector min_v, max_v;
    
    Point min, max;
    
    double rmin, rmax;
    int res_x, res_y;

    public void setMin(Point min)
    {
        this.min = min;
    }

    public void setMax(Point max)
    {
        this.max = max;
    }
    
    public void setResolution(int r)
    {
        this.res_x = r;
        this.res_y = r;
    }
    
    
    public CircleGrid(int numCircles, 
            Point min, Point max, int res_x, int res_y,
            Vector min_v, Vector max_v, double rmin, double rmax)
    {
        circles = new ArrayList<Circle>();
        this.min = min;
        this.max = max;
        this.res_x = res_x;
        this.res_y = res_y;
        this.min_v = min_v;
        this.max_v = max_v;
        this.rmin = rmin;
        this.rmax = rmax;
        
        double extx = max.x - min.x;
        double exty = max.y - min.y;
        double vextx = max_v.x - min_v.x;
        double vexty = max_v.y - min_v.y;
        double rext = rmax - rmin;
        
        for(int i = 0; i < numCircles; ++i)
        {
            double r = rmin + Math.random() * rext;
            
            double px = r + min.x + Math.random() * (extx - r);
            double py = r + min.y + Math.random() * (exty - r);
            
            double vx = min_v.x + Math.random() * vextx;
            double vy = min_v.y + Math.random() * vexty;
            
            circles.add(
                    new Circle(new Point(px, py), r,
                    new Vector(vx, vy)));
        }
        
    }
    
    private double eval_metaballs(double x, double y)
    {
        double sum = 0;
        
        for(Circle c : circles)
            sum += Math.pow(c.r, 2) / (Math.pow(x - c.c.x, 2) + Math.pow(y - c.c.y, 2));
        
        return sum;
    }
    
    private List<SegmentLine> getMarchingSquare(int num, Point min, Point max)
    {
        double mextx = (max.x - min.x)/2;
        double mexty = (max.y - min.y)/2;
        
        List<SegmentLine> marching_square = new ArrayList<SegmentLine>();
        
        switch(num)
        {
            case 0:
                break;
            case 1:
                marching_square.add(new SegmentLine(
                        new Point(min.x + mextx, min.y), 
                        new Point(min.x, min.y + mexty)));
                break;
            case 2:
                marching_square.add(new SegmentLine(
                       new Point(max.x, min.y + mexty),
                       new Point(min.x + mextx, min.y)));
                break;
            case 3:
                marching_square.add(new SegmentLine(
                       new Point(max.x, min.y + mexty), 
                       new Point(min.x, min.y + mexty)));
                break;
            case 4:
                marching_square.add(new SegmentLine(
                       new Point(min.x + mextx, max.y),  
                       new Point(max.x, min.y + mexty))); 
                break;
            case 5:
                marching_square.add(new SegmentLine(
                       new Point(min.x + mextx, max.y), 
                       new Point(min.x, min.y + mexty)));
                marching_square.add(new SegmentLine(
                       new Point(min.x + mextx, min.y), 
                       new Point(max.x, min.y + mexty)));
                break;
            case 6:
                marching_square.add(new SegmentLine(
                       new Point(min.x + mextx, max.y), 
                       new Point(min.x + mextx, min.y)));
                break;
            case 7:
                marching_square.add(new SegmentLine(
                       new Point(min.x + mextx, max.y), 
                       new Point(min.x, min.y + mexty)));
                break;
            case 8:
                marching_square.add(new SegmentLine(
                       new Point(min.x, min.y + mexty), 
                       new Point(min.x + mextx, max.y)));
                break;
            case 9:
                marching_square.add(new SegmentLine(
                       new Point(min.x + mextx, min.y), 
                       new Point(min.x + mextx, max.y)));
                break;
            case 10:
                marching_square.add(new SegmentLine(
                       new Point(min.x, min.y + mexty), 
                       new Point(min.x + mextx, min.y)));
                marching_square.add(new SegmentLine(
                       new Point(max.x, min.y + mexty), 
                       new Point(min.x + mextx, max.y)));
                break;
            case 11:
                marching_square.add(new SegmentLine(
                       new Point(max.x, min.y + mexty), 
                       new Point(min.x + mextx, max.y)));
                break;
            case 12:
                marching_square.add(new SegmentLine(
                       new Point(min.x, min.y + mexty), 
                       new Point(max.x, min.y + mexty)));
                break;
            case 13:
                marching_square.add(new SegmentLine(
                       new Point(min.x + mextx, min.y), 
                       new Point(max.x, min.y + mexty)));
                break;
            case 14:
                marching_square.add(new SegmentLine(
                       new Point(min.x, min.y + mexty), 
                       new Point(min.x + mextx, min.y)));
                break;
            case 15:
                break;
        }
        
        return marching_square;
    }
    
    private List<SegmentLine> getMarchingSquare(double no, double ne,
            double se, double so, Point min, Point max)
    {
        double extx = (max.x - min.x);
        double exty = (max.y - min.y);
        
        List<SegmentLine> marching_square = new ArrayList<SegmentLine>();
        
        int bit_no = no >= 1 ? 1 : 0;
        int bit_ne = ne >= 1 ? 1 : 0;
        int bit_se = se >= 1 ? 1 : 0;
        int bit_so = so >= 1 ? 1 : 0;
        
        int num = bit_no*8 + bit_ne*4 + bit_se*2 + bit_so;
        
        Point E = new Point(max.x, min.y + exty * ( (1 - se)/(ne - se) ));
        Point O = new Point(min.x, min.y + exty * ( (1 - so)/(no - so) ));
        
        Point N = new Point(min.x + extx * ( (1 - no)/(ne - no) ), max.y);
        Point S = new Point(min.x + extx * ( (1 - so)/(se - so) ), min.y);
        
        switch(num)
        {
            case 0:
                break;
            case 1:
                marching_square.add(new SegmentLine(S, O));
                break;
            case 2:
                marching_square.add(new SegmentLine(E, S));
                break;
            case 3:
                marching_square.add(new SegmentLine(E, O));
                break;
            case 4:
                marching_square.add(new SegmentLine(N, E)); 
                break;
            case 5:
                marching_square.add(new SegmentLine(N, O));
                marching_square.add(new SegmentLine(S, E));
                break;
            case 6:
                marching_square.add(new SegmentLine(N, S));
                break;
            case 7:
                marching_square.add(new SegmentLine(N, O));
                break;
            case 8:
                marching_square.add(new SegmentLine(O, N));
                break;
            case 9:
                marching_square.add(new SegmentLine(S, N));
                break;
            case 10:
                marching_square.add(new SegmentLine(O, S));
                marching_square.add(new SegmentLine(E, N));
                break;
            case 11:
                marching_square.add(new SegmentLine(E, N));
                break;
            case 12:
                marching_square.add(new SegmentLine(O, E));
                break;
            case 13:
                marching_square.add(new SegmentLine(S, E));
                break;
            case 14:
                marching_square.add(new SegmentLine(O, S));
                break;
            case 15:
                break;
        }
        
        return marching_square;
    }
    
    public void addCircle()
    {
        double extx = max.x - min.x;
        double exty = max.y - min.y;
        double vextx = max_v.x - min_v.x;
        double vexty = max_v.y - min_v.y;
        double rext = rmax - rmin;
       
        double r = rmin + Math.random() * rext;

        double px = r + min.x + Math.random() * (extx - r);
        double py = r + min.y + Math.random() * (exty - r);

        double vx = min_v.x + Math.random() * vextx;
        double vy = min_v.y + Math.random() * vexty;
            
        circles.add(
                new Circle(new Point(px, py), r,
                new Vector(vx, vy)));

    }
    
    public void removeCircle()
    {
        if(!circles.isEmpty())
            circles.remove(0);
    }
    
    public void moveCircles()
    {
        for(Circle c : circles)
        {
            c.c.x += c.v.x;
            c.c.y += c.v.y;
            
            if(c.c.x + c.r >= max.x) c.v.x = -Math.abs(c.v.x);
            if(c.c.y + c.r >= max.y) c.v.y = -Math.abs(c.v.y);
            
            if(c.c.x - c.r <= min.x) c.v.x = Math.abs(c.v.x);
            if(c.c.y - c.r <= min.y) c.v.y = Math.abs(c.v.y);
        }
    }
    
    public void drawCircles(GL g)
    {
        DrawCircle dc = new DrawCircle();
        for(Circle c : circles)
        {           
            dc.setCircle(c);
            dc.drawObjectCircle(g, 1, 1, 1);
        }
    }
    
    public void drawGrid(GL g)
    {
        double extx = max.x - min.x;
        double exty = max.y - min.y;
        double incrx = extx / res_x;
        double incry = exty / res_y;
        
        double x1 , y1, x2, y2;
        g.glBegin(GL.GL_LINES);
        g.glColor3d(0.5, 0.5, 0.5);
        for(int i = 0; i <= res_x; ++i)
        {
            x1 = min.x + i * incrx; y1 = max.y;
            x1 = convCoordX(x1);    y1 = convCoordX(y1);
            g.glVertex2d(x1, y1);
            
            x2 = min.x + i * incrx; y2 = min.y;
            x2 = convCoordX(x2);    y2 = convCoordX(y2);
            g.glVertex2d(x2, y2);

        }
        
        for(int i = 0; i < res_y; ++i)
        {
            
            x1 = min.x; y1 = max.y - i * incry;
            x1 = convCoordX(x1);    y1 = convCoordX(y1);
            g.glVertex2d(x1, y1);
            
            x2 = max.x; y2 = max.y - i * incry;
            x2 = convCoordX(x2);    y2 = convCoordX(y2);
            g.glVertex2d(x2, y2);
    
        }
        g.glEnd();
    }
    
    public void drawGridSamples(GL g)
    {
        drawGridSamples(g, Color.GREEN);
    }
    
    public void drawGridSamples(GL g, Color col)
    {
        float cr = (float)(1.0 / 255) * col.getRed();
        float cg = (float)(1.0 / 255) * col.getGreen();
        float cb = (float)(1.0 / 255) * col.getBlue();
        
        double extx = max.x - min.x;
        double exty = max.y - min.y;
        double incrx = extx / res_x;
        double incry = exty / res_y;
        
        double x1, y1;
        
        g.glColor3d(1, 1, 1);
        g.glPointSize(WIDTH / (2 * res_x + 50));
        
        g.glBegin(GL.GL_POINTS);
        for(int i = 0; i < res_x; ++i)
        {
            for(int j = 0; j < res_y; ++j)
            {
                
                x1 = min.x + i * incrx + 0.5 * incrx; 
                y1 = max.y - j * incry - 0.5 * incry;
                
                if(eval_metaballs(x1, y1) >= 1)
                    g.glColor3d(cr, cg, cb);
                else
                    g.glColor3d(1, 1, 1);
                
                x1 = convCoordX(x1);    y1 = convCoordX(y1);
                g.glVertex2d(x1, y1);

            }
        }
        g.glEnd();
    }
    
    public void drawCornersSamples(GL g)
    {
        drawCornersSamples(g, Color.GREEN);
    }
    
    public void drawCornersSamples(GL g, Color col)
    {
        float cr = (float)(1.0 / 255) * col.getRed();
        float cg = (float)(1.0 / 255) * col.getGreen();
        float cb = (float)(1.0 / 255) * col.getBlue();
        
        double extx = max.x - min.x;
        double exty = max.y - min.y;
        double incrx = extx / res_x;
        double incry = exty / res_y;
        
        double x1, y1;
        
        g.glColor3d(1, 1, 1);
        g.glPointSize(WIDTH / (2 * res_x + 50));
        
        g.glBegin(GL.GL_POINTS);
        for(int i = 0; i <= res_x; ++i)
        {
            for(int j = 0; j <= res_y; ++j)
            {
                
                x1 = min.x + i * incrx; 
                y1 = max.y - j * incry;
                
                if(eval_metaballs(x1, y1) >= 1)
                    g.glColor3d(cr, cg, cb);
                else
                    g.glColor3d(1, 1, 1);
                
                x1 = convCoordX(x1);    y1 = convCoordX(y1);
                g.glVertex2d(x1, y1);

            }
        }
        g.glEnd();
    }
    
    public void drawGridQuadSamples(GL g)
    {
        drawGridQuadSamples(g, Color.GREEN);
    }
    
    public void drawGridQuadSamples(GL g, Color col)
    {
        float cr = (float)(1.0 / 255) * col.getRed();
        float cg = (float)(1.0 / 255) * col.getGreen();
        float cb = (float)(1.0 / 255) * col.getBlue();
        
        double extx = max.x - min.x;
        double exty = max.y - min.y;
        double incrx = extx / res_x;
        double incry = exty / res_y;
        
        double x, y, x1, y1, x2, y2, x3, y3, x4, y4;
        
        g.glColor3d(cr, cg, cb);
        
        g.glBegin(GL.GL_QUADS);
        for(int i = 0; i < res_x; ++i)
        {
            for(int j = 0; j < res_y; ++j)
            {
                
                x = min.x + i * incrx + 0.5 * incrx; 
                y = max.y - j * incry - 0.5 * incry;

                if (eval_metaballs(x, y) >= 1)
                {
                    x1 = min.x + i * incrx;
                    y1 = max.y - j * incry;

                    x2 = min.x + i * incrx;
                    y2 = max.y - j * incry - incry;

                    x3 = min.x + i * incrx + incrx;
                    y3 = max.y - j * incry - incry;

                    x4 = min.x + i * incrx + incrx;
                    y4 = max.y - j * incry;
                    
                    x1 = convCoordX(x1); y1 = convCoordX(y1);
                    x2 = convCoordX(x2); y2 = convCoordX(y2);
                    x3 = convCoordX(x3); y3 = convCoordX(y3);
                    x4 = convCoordX(x4); y4 = convCoordX(y4);
                    
                    g.glVertex2d(x1, y1);
                    g.glVertex2d(x2, y2);
                    g.glVertex2d(x3, y3);
                    g.glVertex2d(x4, y4);
                }
            }
        }
        g.glEnd();
    }
    
    public void drawMarchingSquares(GL g)
    {
        drawMarchingSquares(g, Color.GREEN);
    }
    
    public void drawMarchingSquares(GL g, Color col)
    {
        float cr = (float)(1.0 / 255) * col.getRed();
        float cg = (float)(1.0 / 255) * col.getGreen();
        float cb = (float)(1.0 / 255) * col.getBlue();
        
        double extx = max.x - min.x;
        double exty = max.y - min.y;
        double incrx = extx / res_x;
        double incry = exty / res_y;
        
        DrawSegment ds = new DrawSegment();
        double x1, y1;
        double sample_matrix[][] = new double[res_x + 1][res_y + 1];
        for (int i = 0; i <= res_x; ++i)
        {
            for (int j = 0; j <= res_y; ++j)
            {

                x1 = min.x + i * incrx;
                y1 = max.y - j * incry;
                
                sample_matrix[i][j] = eval_metaballs(x1, y1);
            }
        }
        
        for (int i = 0; i < res_x; ++i)
        {
            for (int j = 0; j < res_y; ++j)
            {

                x1 = min.x + i * incrx;
                y1 = max.y - j * incry;
                
                int no = sample_matrix[i][j]     >= 1 ? 1 : 0;
                int ne = sample_matrix[i+1][j]   >= 1 ? 1 : 0;
                int se = sample_matrix[i+1][j+1] >= 1 ? 1 : 0;
                int so = sample_matrix[i][j+1]   >= 1 ? 1 : 0;
                
                int num = no*8 + ne*4 + se*2 + so;
                List<SegmentLine> marching_square = getMarchingSquare(num, 
                        new Point(x1, y1 - incry), new Point(x1 + incrx, y1));
                
                for(SegmentLine s : marching_square)
                {
                    ds.setSegment(s);
                    ds.drawObjectC(g, cr, cg, cb);
                }
                
            }
        }
        
        
    }
    
    public void drawLerpMarchingSquares(GL g)
    {
        drawLerpMarchingSquares(g, Color.GREEN);
    }
    
    public void drawLerpMarchingSquares(GL g, Color col)
    {
        float cr = (float)(1.0 / 255) * col.getRed();
        float cg = (float)(1.0 / 255) * col.getGreen();
        float cb = (float)(1.0 / 255) * col.getBlue();
        
        double extx = max.x - min.x;
        double exty = max.y - min.y;
        double incrx = extx / res_x;
        double incry = exty / res_y;
        
        DrawSegment ds = new DrawSegment();
        double x1, y1;
        double sample_matrix[][] = new double[res_x + 1][res_y + 1];
        for (int i = 0; i <= res_x; ++i)
        {
            for (int j = 0; j <= res_y; ++j)
            {

                x1 = min.x + i * incrx;
                y1 = max.y - j * incry;
                
                sample_matrix[i][j] = eval_metaballs(x1, y1);
            }
        }
        
        for (int i = 0; i < res_x; ++i)
        {
            for (int j = 0; j < res_y; ++j)
            {

                x1 = min.x + i * incrx;
                y1 = max.y - j * incry;
                
                double no = sample_matrix[i][j]    ;
                double ne = sample_matrix[i+1][j]  ;
                double se = sample_matrix[i+1][j+1];
                double so = sample_matrix[i][j+1]  ;
                
                List<SegmentLine> marching_square = getMarchingSquare(
                        no, ne, se, so, 
                        new Point(x1, y1 - incry), new Point(x1 + incrx, y1));
                
                for(SegmentLine s : marching_square)
                {
                    ds.setSegment(s);
                    ds.drawObjectC(g, cr, cg, cb);
                }
                
            }
        }
        
        
    }

    @Override
    public void drawObject(GL gl)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void drawObjectC(GL gl, float R, float G, float B)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
