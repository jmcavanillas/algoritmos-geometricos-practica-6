package algGeom.lib2D;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.media.opengl.*;

/**
 * Class for drawing a line
 */
public class DrawLine extends Draw {

    Line vl;

    public DrawLine(Line l) {
        vl = l;
    }

    public Line getLine() {
        return vl;
    }

    @Override
    public void drawObject(GL g) {

        // screen coordiantes
        double ax = 0, ay = 0, bx = 0, by = 0;
        double m = vl.slope();
        double c = vl.getC();
        
        /*
        if (m < BasicGeom.INFINITY) { //intersects with the lateral canvas

        } else {

            //XXXXX
        }
        */
        Point p;
        try {
            p = vl.getPoint(1000);
            ax = p.x;
            ay = p.y;
            p = vl.getPoint(-1000);
            bx = p.x;
            by = p.y;
        } catch (SegmentLine.Invalid_T_Parameter ex) {
            Logger.getLogger(DrawLine.class.getName()).log(Level.SEVERE, null, ex);
        }

        ax = convCoordX(ax);
        ay = convCoordX(ay);
        bx = convCoordX(bx);
        by = convCoordX(by);

        g.glBegin(GL.GL_LINES);
        g.glVertex2d(ax, ay);
        g.glVertex2d(bx, by); //the fourth (w) component is zero!
        g.glEnd();

    }

    @Override
    public void drawObjectC(GL g, float R, float G, float B) {

        g.glLineWidth(1.5f);
        g.glColor3f(R, G, B);
        drawObject(g);
    }

}
