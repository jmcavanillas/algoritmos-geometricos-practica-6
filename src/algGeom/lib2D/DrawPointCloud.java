package algGeom.lib2D;

import javax.media.opengl.*;

public class DrawPointCloud extends Draw {

    PointCloud pc;

    public DrawPointCloud(PointCloud p) {
        pc = p;
    }

    public PointCloud getPointCloud() {
        return pc;
    }

    @Override
    public void drawObject(GL g) {

        double x, y;

        g.glPointSize(5.0f);
        g.glBegin(GL.GL_POINTS);

        for (Point p : pc.points) {
            x = p.x;
            y = p.y;
            x = convCoordX(x);
            y = convCoordY(y);
            g.glVertex2d(x, y);
        }
        g.glEnd();

    }

    @Override
    public void drawObjectC(GL g, float R, float G, float B) {

        g.glColor3f(R, G, B);
        this.drawObject(g);
    }

}
