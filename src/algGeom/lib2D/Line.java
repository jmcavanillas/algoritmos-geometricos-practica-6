package algGeom.lib2D;
 
import Util.MutableDouble;


/**
 * Class that represents a line in the plane
 */
public class Line extends SegmentLine {

    public Line(Point a, Point b) {
        super(a, b);
    }

    public Line(SegmentLine s) {
        a = s.a;
        b = s.b;
    }
    
    public boolean intersects(Line r, Vector interseccion)
    {
        MutableDouble s = new MutableDouble(0.0), t = new MutableDouble(0.0);
        if (intersects(new Vector(r.a), new Vector(r.b), s, t))
        {
            try
            {
                interseccion.x = new Vector(getPoint(s.getValue())).x;
                interseccion.y = new Vector(getPoint(s.getValue())).y;
                
            } catch (Invalid_T_Parameter e){}

            return true;
        }
        return false;
    }
    
    public boolean intersects(RayLine r, Vector interseccion)
    {
        MutableDouble s = new MutableDouble(0.0), t = new MutableDouble(0.0);
        if (intersects(new Vector(r.a), new Vector(r.b), s, t))
        {
            try
            {

                interseccion.x = new Vector(getPoint(s.getValue())).x;
                interseccion.y = new Vector(getPoint(s.getValue())).y;
                
            } catch (Invalid_T_Parameter e){}
            
            if (t.getValue() >= 0)
                return true;
        }
        return false;
    }
    
    public boolean intersects(SegmentLine r, Vector interseccion)
    {
        MutableDouble s = new MutableDouble(0.0), t = new MutableDouble(0.0);
        if (intersects(new Vector(r.a), new Vector(r.b), s, t))
        {
            try
            {
                interseccion.x = new Vector(getPoint(s.getValue())).x;
                interseccion.y = new Vector(getPoint(s.getValue())).y;
                
            } catch (Invalid_T_Parameter e){}
            
            if (t.getValue() >= 0 && t.getValue() <= 1)
                return true;
        }
        return false;
    }
    
    double distPointLine(Vector A)
    {
        Vector P = new Vector(a);
        Vector d = new Vector(b).sub(P);
        
        Double t = d.dot(A.sub(P)) / d.dot(d);
        
        return A.sub(P.add(d.scalarMult(t))).getModule();
    }

    @Override
    public boolean segmentIntersection(SegmentLine l) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean impSegmentIntersection(SegmentLine l) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected void check_t(double t) throws Invalid_T_Parameter {
    }
    
    
    public double distance(final Vector v) {
        //XXXXX
        return 0.0;
    }

    
    @Override
    public void out() {
        System.out.println("Line->");
        System.out.println("Point a: ");
        a.out();
        System.out.println("Point b: ");
        b.out();
    }

}
