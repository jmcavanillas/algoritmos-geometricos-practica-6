package algGeom.lib2D;

import java.io.BufferedReader;
import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

class SaveIOException extends Exception {

    private static final long serialVersionUID = 1L;
}

class ReadIOException extends Exception {

    private static final long serialVersionUID = 2L;
}

/**
 * Class that represents a point cloud
 */
public class PointCloud {

    ArrayList<Point> points;

    public PointCloud() {
        points = new ArrayList<Point>();

    }
    
    public PointCloud(PointCloud orig) {
        points = orig.points;

    }

    /**
     * Constructor of the point cloud of random form giving the total number of
     * points.
     */
    public PointCloud(int tam) {
        points = new ArrayList<Point>();
        Random rand = new Random();
        for (int i = 0; i < tam; i++) {
            int x = rand.nextInt(150);
            int y = rand.nextInt(150);
            points.add(new Point(x - 75, y - 75));
        }
    }

    /**
     * Constructor of the point cloud from the coordinates of points stored in
     * file
     */
    public PointCloud(String nombre) throws FileNotFoundException {
        points = new ArrayList<Point>();
        File file = new File(nombre);
        Scanner sc = new Scanner(file);

        String[] coords;

        while (sc.hasNext()) {
            coords = sc.next().split("/");
            points.add(new Point(Double.parseDouble(coords[0]),
                    Double.parseDouble(coords[1])));
        }
    }

    public void addPoint(Point p) {
        points.add(p);
    }

    public int size() {
        return points.size();
    }

    /**
     * Saves the cloud of points in file with the same format used by the
     * constructor
     *
     * @param nombre
     * @throws algGeom.lib2D.SaveIOException
     * @throws java.io.IOException
     */
    public void save(String nombre) throws SaveIOException, IOException {
        String info = "";
        for (Point point : points) {
            info = info.concat(Double.toString(point.x)
                    + "/" + Double.toString(point.y));
            info += System.lineSeparator();
        }
        BufferedWriter writer = new BufferedWriter(new FileWriter(nombre));
        writer.write(info);
        writer.close();
    }

    public Point getPoint(int pos) {
        if ((pos >= 0) && (pos < points.size())) {
            return points.get(pos);
        }
        return null;
    }

    /**
     * Returns the more central existing point in the cloud
     */
    public Point centralPoint() {

        double dist = Double.MAX_VALUE;
        int pos = 0;

        for (int i = 0; i < points.size(); i++) {
            double dMax = 0;
            for (int j = 0; j < points.size(); j++) {
                if (dMax > points.get(j).distance(points.get(i))) {
                    dMax = points.get(j).distance(points.get(i));
                }
            }
            if (dMax < dist ) {
                pos = i;
                dist = dMax;
            }
        }

        return points.get(pos);
    }

}
