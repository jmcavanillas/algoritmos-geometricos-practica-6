/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algGeom.lib2D;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.triangulate.DelaunayTriangulationBuilder;
import com.vividsolutions.jts.triangulate.IncrementalDelaunayTriangulator;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Javier Mart�nez Cavanillas
 */
public class TDelaunay
{
    final static GeometryFactory GEOM_FACT = new GeometryFactory();
    
    final private DelaunayTriangulationBuilder geomBuilder;
    
    private PointCloud points;
    private List<SegmentLine> net;
    
    public TDelaunay()
    {
        this(new PointCloud(20));
    }
    
    public TDelaunay(TDelaunay orig)
    {
        this(new PointCloud(orig.points));
    }
    
    public TDelaunay(int tam)
    {
        this(new PointCloud(tam));
    }
    
    
    public TDelaunay(String path) throws FileNotFoundException
    {
        this(new PointCloud(path));
    }
    
    public TDelaunay(PointCloud pc)
    {
        geomBuilder = new DelaunayTriangulationBuilder();
        net = new ArrayList<SegmentLine>();
        
        points = pc;
        
        ArrayList<Coordinate> pointList = new ArrayList<Coordinate>();
        
        for (int i = 0; i < pc.size(); ++i)
        {
            Point p = pc.getPoint(i);
            pointList.add(new Coordinate(p.x, p.y));
        }
        
        geomBuilder.setSites(pointList);
        
        Geometry g = geomBuilder.getEdges(GEOM_FACT);
        MultiLineString edges = (MultiLineString) g;
        
        for(int i = 0; i < edges.getNumGeometries(); ++i)
        {
            LineString line = (LineString) edges.getGeometryN(i);
            net.add(new SegmentLine(
                    line.getPointN(0).getX(), line.getPointN(0).getY(),
                    line.getPointN(1).getX(), line.getPointN(1).getY()
            ));
        }

    }
    
    List<SegmentLine> getEdges()
    {
        return net;
    }
    
    public void addPoint(Point p)
    {
        IncrementalDelaunayTriangulator idt = 
                new IncrementalDelaunayTriangulator(geomBuilder.getSubdivision());
        
        idt.insertSite(new com.vividsolutions.jts.triangulate.quadedge.Vertex(p.x, p.y));
        
        Geometry g = geomBuilder.getEdges(GEOM_FACT);
        MultiLineString edges = (MultiLineString) g;
        net.clear();
        
        for(int i = 0; i < edges.getNumGeometries(); ++i)
        {
            LineString line = (LineString) edges.getGeometryN(i);
            net.add(new SegmentLine(
                    line.getPointN(0).getX(), line.getPointN(0).getY(),
                    line.getPointN(1).getX(), line.getPointN(1).getY()
            ));
        }
    }
}
