/**
 * Java Library for Geometric Algorithms subject
 *
 * @author Lidia Ortega, Alejandro Graciano
 * @version 1.0
 */
package algGeom.lib2D;

import com.sun.opengl.util.Animator;
import Util.RandomGen;
import java.awt.Frame;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.TextField;

import javax.media.opengl.*;
import java.awt.event.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JSlider;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class Test extends Frame implements GLEventListener {

    static int A = 0;
    
    static int HEIGHT = 800;
    static int WIDTH = 800;
    static int POINT_CLOUD_VERT = 20;

    static GL gl; // interface to OpenGL
    static GLCanvas canvas; // drawable in a frame
    static GLCapabilities capabilities;
    
    static Color color;
    static float lineWidth;
    
    static UI UI;
    static ColorPicker cpicker;

    static boolean visualizeAxis = false;
    static int exercise = A;
    static Animator animator;
    
    private CircleGrid circleGridA;
    private CircleGrid circleGridB;
    private CircleGrid circleGridC;
    

    public Test(String title) {
        super(title);
        
        lineWidth = 1.0f;
        color = Color.GREEN;
        cpicker = new ColorPicker();
        cpicker.setTitle("Choose a Color");
        cpicker.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        cpicker.cc.setColor(color);
        
        
        UI = new UI();
        add(UI, BorderLayout.WEST);

        // 1. specify a drawable: canvas
        capabilities = new GLCapabilities();
        capabilities.setDoubleBuffered(false);
        canvas = new GLCanvas();
        canvas.setSize(WIDTH, HEIGHT);

        // 2. listen to the events related to canvas: reshape
        canvas.addGLEventListener(this);

        // 3. add the canvas to fill the Frame container
        add(canvas, BorderLayout.CENTER);

        // 4. interface to OpenGL functions
        gl = canvas.getGL();
        gl.glEnable(GL.GL_LINE_WIDTH);
        

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
//				animator.stop(); // stop animation
                System.exit(0);
            }
        });

        canvas.addKeyListener(new KeyListener() {
            long clock = 0;

            /**
             * Handle the key typed event from the text field.
             */
            public void keyTyped(KeyEvent e) {
//                        System.out.println(e + "KEY TYPED: ");
            }

            /**
             * Handle the key pressed event from the text field.
             */
            public void keyPressed(KeyEvent e) {
                switch (e.getKeyChar()) {               
                    case 27: // esc
                        System.exit(0);
                }

            }

            /**
             * Handle the key released event from the text field.
             */
            public void keyReleased(KeyEvent e) {
                clock = e.getWhen();
//                        System.out.println(e + "KEY RELEASED: ");
            }
        });
        
        
        /* ----------- Add UIListeners ---------------- */
        
        UI.SliderCircles.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                JSlider slider = (JSlider) e.getSource();
                if(slider.getValue() > circleGridA.circles.size())
                    circleGridA.addCircle();
                if(slider.getValue() < circleGridA.circles.size())
                    circleGridA.removeCircle();
                    
            }
        });
        
        UI.SliderResolution.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                JSlider slider = (JSlider) e.getSource();
                circleGridA.setResolution(slider.getValue() * 10);
            }
        });
        
        UI.SliderLineWidth.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                JSlider slider = (JSlider) e.getSource();
                lineWidth = slider.getValue();
            }
        });
        
        UI.ChooseColorBtn.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                cpicker.setVisible(true);
            }
        });
        
        cpicker.cc.getSelectionModel().addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                color = cpicker.cc.getColor();
            }
        });
    }

    protected void initExerciseA() {
        circleGridA = new CircleGrid(14, 
                new Point(-100,-100), new Point(100, 100), 20, 20,
                new Vector(0.1, 0.1), new Vector(0.6, 0.6), 2, 20);
    }

    protected void drawExerciseA() {
        gl.glLineWidth(lineWidth);
        
        if(UI.ShowGrid.isSelected()) circleGridA.drawGrid(gl);
        if(UI.SamplePoints.isSelected()) circleGridA.drawGridSamples(gl, color);
        if(UI.CornerSamplePoints.isSelected()) circleGridA.drawCornersSamples(gl, color);
        if(UI.ColorCells.isSelected()) circleGridA.drawGridQuadSamples(gl, color);
        if(UI.Circles.isSelected()) circleGridA.drawCircles(gl);
        if(UI.MSquares.isSelected()) circleGridA.drawMarchingSquares(gl, color);
        if(UI.MSquaresLerp.isSelected()) circleGridA.drawLerpMarchingSquares(gl, color);

        
        circleGridA.moveCircles();
    }

    // called once for OpenGL initialization
    public void init(GLAutoDrawable drawable) {

        animator = new Animator(canvas);
        animator.start(); // start animator thread
        // display OpenGL and graphics system information
        System.out.println("INIT GL IS: " + gl.getClass().getName());
        System.err.println(drawable.getChosenGLCapabilities());
        System.err.println("GL_VENDOR: " + gl.glGetString(GL.GL_VENDOR));
        System.err.println("GL_RENDERER: " + gl.glGetString(GL.GL_RENDERER));
        System.err.println("GL_VERSION: " + gl.glGetString(GL.GL_VERSION));

        RandomGen random = RandomGen.getInst();
        //random.setSeed(5308170449555L);
        System.err.println("SEED: " + random.getSeed());

        try {

            initExerciseA();

        } catch (Exception ex) {
            System.out.println("Error en el dibujado");
            ex.printStackTrace();
        }
        
        /* ----------- Initialize UI Elements --------- */
        
        UI.SliderCircles.setValue(circleGridA.circles.size());
        UI.SliderResolution.setValue(circleGridA.res_x / 10);
        UI.SliderLineWidth.setValue(1);
        
        canvas.requestFocus();

    }

    public void reshape(GLAutoDrawable drawable, int x, int y, int width,
            int height) {

        Draw.WIDTH = WIDTH = width; // new width and height saved
        Draw.HEIGH = HEIGHT = height;
        //DEEP = deep;
        if (Draw.HEIGH < Draw.WIDTH) {
            Draw.WIDTH = Draw.HEIGH;
        }
        if (Draw.HEIGH > Draw.WIDTH) {
            Draw.HEIGH = Draw.WIDTH;
        }
        // 7. specify the drawing area (frame) coordinates
        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();
        gl.glOrtho(0, width, 0, height, -100, 100);
        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glLoadIdentity();
        if (HEIGHT < WIDTH) {
            gl.glTranslatef((WIDTH - HEIGHT) / 2, 0, 0);
        }
        if (HEIGHT > WIDTH) {
            gl.glTranslatef(0, (HEIGHT - WIDTH) / 2, 0);
        }

    }

    // called for OpenGL rendering every reshape
    public void display(GLAutoDrawable drawable) {

        // limpiar la pantalla
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        /* El color de limpiado ser cero */
        gl.glClearDepth(1.0);
        
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

        if (visualizeAxis) {
            DrawAxis ejes = new DrawAxis();
            ejes.drawObject(gl);
        }
        
        if (exercise == A) {
            drawExerciseA();
        }

        gl.glFlush();
    }

    // called if display mode or device are changed
    public void displayChanged(GLAutoDrawable drawable, boolean modeChanged,
            boolean deviceChanged) {
    }

    public static void main(String[] args) {
        
        try
        {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException ex)
        {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex)
        {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex)
        {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex)
        {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Draw.HEIGH = HEIGHT;
        Draw.WIDTH = WIDTH;

        Test frame = new Test("Practice 6 - Metaballs & Marching Squares");
        frame.pack();
        frame.setVisible(true);
    }
}
