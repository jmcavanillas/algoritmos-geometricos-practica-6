package algGeom.lib3D;

import Util.MollerTriAABB;

public class AABB {

    Vect3d min; //menor x,y,z
    Vect3d max; //max x, y, z

    public void setMin(Vect3d min) {
        this.min = min;
    }

    public void setMax(Vect3d max) {
        this.max = max;
    }

    public AABB(Vect3d min, Vect3d max) {
        this.min = min;
        this.max = max;
    }

    /**
     * get the extension vector
     */
    public Vect3d getExtent() {
        return max.sub(min).scalarMul(0.5);
    }

    /**
     * get the cube center
     */
    public Vect3d getCenter() {
        Vect3d center = max.add(min).scalarMul(0.5);
        return center;
    }

    public Vect3d getMin() {
        return min;
    }

    public Vect3d getMax() {
        return max;
    }

    public boolean pointAABB(Vect3d p) {
        if (p.x < max.x && p.x >= min.x) {
            if (p.y < max.y && p.y >= min.y) {
                if (p.z < max.z && p.z >= min.z) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean AABB_tri(Triangle3d tri) {
        return MollerTriAABB.triBoxOverlap(getCenter(), getExtent(), tri);
    }

}
