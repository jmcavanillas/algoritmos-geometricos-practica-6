/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algGeom.lib3D;

import Util.Draw;
import java.util.ArrayList;
import java.util.Arrays;
import javax.media.opengl.GL;

/**
 *
 * @author Alberto
 */
public class DrawOctree extends Draw {

    Octree oc;

    public DrawOctree(Octree oc) {
        this.oc = oc;
    }

    @Override
    public void drawObject(GL g) {
        DrawAABB draw;
        OctreeNode[] nodos = oc.getRoot().getChildren();

        ArrayList<OctreeNode> aux = new ArrayList();
        ArrayList<OctreeNode> result = new ArrayList();
        aux.addAll(Arrays.asList(nodos));

        while (!aux.isEmpty()) {
            OctreeNode n = aux.get(0);
            aux.remove(0);
            if (n.hasChildren()) {
                aux.addAll(Arrays.asList(n.getChildren()));
            } else {
                result.add(n);
            }

        }

        for (OctreeNode nodo : result) {
            draw = new DrawAABB(nodo.getAABB());
            switch (nodo.color) {
                case BLACK:
                    g.glColor3f(0.3f, 0.3f, 0.3f);
                    break;
                case WHITE:
                    g.glColor3f(1, 1, 1);
                    break;
                case GREY:
                    g.glColor3f(0.7f, 0.7f, 0.7f);
                    break;
            }
            draw.drawObject(g);
        }
    }

    @Override
    public void drawObjectC(GL g, float R, float G, float B) {
        g.glColor3f(R, G, B);
    }

}
