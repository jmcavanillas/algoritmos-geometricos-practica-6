package algGeom.lib3D;

import Util.*;
import javax.media.opengl.*;

public class DrawRay3d extends Draw {

    Ray3d vr;

    public DrawRay3d(Ray3d r) {
        vr = r;
    }

    public Ray3d getRay3d() {
        return vr;
    }

    @Override
    public void drawObject(GL g) {

        Vect3d low = vr.getPoint(0);
        Vect3d high = vr.getPoint(1000);

        g.glBegin(GL.GL_LINES);
        g.glVertex3d(low.x, low.y, low.z);
        g.glVertex3d(high.x, high.y, high.z);
        g.glEnd();

    }

    @Override
    public void drawObjectC(GL g, float R, float G, float B) {
        g.glColor3f(R, G, B);
        drawObject(g);
    }
}
