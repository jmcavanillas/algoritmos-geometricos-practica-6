package algGeom.lib3D;

import Util.*;
import javax.media.opengl.*;

public class DrawTriangle3d extends Draw {

    Triangle3d tr;
    Vect3d n;

    /**
     * Creates a new instance of VisuPunto
     */
    public DrawTriangle3d(Triangle3d t) {
        tr = t;
        n = tr.normal();
        n = n.scalarMul(1/n.module());
    }

    @Override
    public void drawObject(GL g) {
        g.glBegin(GL.GL_TRIANGLES);
        g.glNormal3f((float) n.x, (float) n.y, (float) n.z);
        g.glVertex3d((float) tr.a.x, (float) tr.a.y, (float) tr.a.z);
        g.glVertex3d((float) tr.b.x, (float) tr.b.y, (float) tr.b.z);
        g.glVertex3d((float) tr.c.x, (float) tr.c.y, (float) tr.c.z);
        g.glEnd();
//        g.glColor4f(0,0,0,0.8f);
//        g.glLineWidth(3);
//        g.glBegin(GL.GL_LINES);
//        g.glVertex3d((float) tr.c.x, (float) tr.c.y, (float) tr.c.z);
//        g.glVertex3d((float) tr.c.x + n.x * 100, (float) tr.c.y + n.y* 100, (float) tr.c.z + n.z* 100);
//        g.glEnd();
    }

    public void drawObjectStrip(GL g) {
        
       g.glColor3f(0.3f, 0, 0.3f);
        g.glBegin(GL.GL_LINE_LOOP);
        g.glNormal3f((float) n.x, (float) n.y, (float) n.z);
        g.glVertex3d((float) tr.a.x, (float) tr.a.y, (float) tr.a.z);
        g.glVertex3d((float) tr.b.x, (float) tr.b.y, (float) tr.b.z);
        g.glVertex3d((float) tr.c.x, (float) tr.c.y, (float) tr.c.z);

        g.glEnd();

    }

    @Override
    public void drawObjectC(GL g, float R, float G, float B) {
        g.glColor3f(R, G, B);
        drawObject(g);
    }

}
