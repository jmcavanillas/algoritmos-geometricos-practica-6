package algGeom.lib3D;

import Util.BasicGeom;





enum classifyLines {NON_INTERSECT, PARALLEL, INTERSECT, THESAME}

public class Line3d extends Edge3d {

    public Line3d(Vect3d o, Vect3d d) {
        super(o, d);
    }
    
    
    /**
     *
     * @return  distance between line/point
     *
     */
    
    public double distance(Vect3d p) {
        Vect3d u = dest.sub(orig);
        Vect3d AP = p.sub(orig);
        
        return u.xProduct(AP).module()/u.module();
    }
   
    
    /**
     *
     * @return  distance between line/line
     *
     */
    public double distance (Line3d l){
        Vect3d sub_q = orig.sub(l.orig);
        Vect3d v_1 = dest.sub(orig);
        Vect3d v_2 = l.dest.sub(l.orig);
        Vect3d cross_v = v_1.xProduct(v_2);
        
        double result = sub_q.dot(cross_v)/cross_v.module();
        
        return Math.abs(result);
    }
    
    /**
     *
     * @return  true if are parallel
     *
     */
    
    public boolean parallel (Line3d l){
        Vect3d a = dest.sub(orig);
        Vect3d b = l.dest.sub(l.orig);
        
        Vect3d cross_p = a.xProduct(b);
        
        boolean result = BasicGeom.equal(cross_p.x, 0) &&
                BasicGeom.equal(cross_p.y, 0) && 
                BasicGeom.equal(cross_p.z, 0);
        
        return result;
    }
    
    /**
     *
     * @return  true if they ara perpendicular 
     *
     */

    public boolean perpendicular (Line3d l){
        Vect3d a = dest.sub(orig);
        Vect3d b = l.dest.sub(l.orig);
        
        return BasicGeom.equal(a.dot(b), 0);
    }

    /**
     *
     * @return  the normal line in the point p 
     *
     */
    
    public Line3d normalLine (Vect3d p){
        Vect3d u = dest.sub(orig);
        u = u.scalarMul(1/u.module());
        double lambda = u.dot(p.sub(orig));
        
        return new Line3d(orig.add(u.scalarMul(lambda)), p);
    }
    
    public void out() {
        System.out.print("Line->Origin: ");
        orig.out();
        System.out.print("Line->Destination: ");
        dest.out();
    }

    
}
