/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algGeom.lib3D;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Javier Mart�nez Cavanillas
 */
public class Octree {

    private static final int DEF_MAX_LVL = 7;
    private static final int DEF_MAX_TRI_PER_NODE = 6;

    private final int MAX_LVL;
    private final int MAX_TRI_PER_NODE;

    private int nTriangles;
    private List<Triangle3d> vTriangles;

    private OctreeNode root;

    public Octree() {
        MAX_LVL = DEF_MAX_LVL;
        MAX_TRI_PER_NODE = DEF_MAX_TRI_PER_NODE;

        nTriangles = 0;
        vTriangles = new ArrayList<Triangle3d>();

        root = new OctreeNode(0, null, new Vect3d(0, 0, 0), new Vect3d(0, 0, 0), this);
    }

    public Octree(TriangleMesh triangleMesh, int max_lvl, int max_tri_per_node) {
        MAX_LVL = max_lvl;
        MAX_TRI_PER_NODE = max_tri_per_node;

        nTriangles = triangleMesh.getFacesSize();
        vTriangles = new ArrayList<Triangle3d>();

        AABB box = triangleMesh.getAABB();

        root = new OctreeNode(0, null, box.getMin(), box.getMax(), this);

        for (int i = 0; i < nTriangles; ++i) {
            Triangle3d t = triangleMesh.getTriangle(i);
            vTriangles.add(t);
            root.insert(t);
        }
    }

    public Octree(TriangleMesh triangleMesh) {
        MAX_LVL = DEF_MAX_LVL;
        MAX_TRI_PER_NODE = DEF_MAX_TRI_PER_NODE;

        nTriangles = triangleMesh.getFacesSize();
        vTriangles = new ArrayList<Triangle3d>();

        AABB box = triangleMesh.getAABB();

        root = new OctreeNode(0, null, box.getMin(), box.getMax(), this);

        for (int i = 0; i < nTriangles; ++i) {
            Triangle3d t = triangleMesh.getTriangle(i);
            vTriangles.add(t);
            root.insert(t);
        }
    }

    public int getMaxLvl() {
        return MAX_LVL;
    }

    public int getMaxTriPerNode() {
        return MAX_TRI_PER_NODE;
    }

    public OctreeNode getRoot() {
        return root;
    }

    public OctreeNode getPoint(Vect3d p) {
        OctreeNode n = root;
        boolean find;
        while (n.hasChildren()) {
            find = false;
            for (OctreeNode octreeNode : n.children) {
                if (octreeNode.getAABB().pointAABB(p)) {
                    n = octreeNode;
                    find = true;
                    break;
                }
            }
            if (!find) {
                return null;
            }
        }
        return n;
    }
}
