/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algGeom.lib3D;

import java.util.ArrayList;
import java.util.List;

enum NodeColor {
    WHITE, BLACK, GREY
}

/**
 *
 * @author Javier Mart�nez Cavanillas
 */
public class OctreeNode {

    NodeColor color;

    protected Octree octree;
    protected OctreeNode parent;
    protected OctreeNode[] children;

    protected List<Triangle3d> contents;
    protected Vect3d min;
    protected Vect3d max;
    protected int lvl;

    private void initChildren() {
        double minX = min.getX();
        double minY = min.getY();
        double minZ = min.getZ();
        double maxX = max.getX();
        double maxY = max.getY();
        double maxZ = max.getZ();
        double medX = (maxX + minX) / 2;
        double medY = (maxY + minY) / 2;
        double medZ = (maxZ + minZ) / 2;

        children[6] = new OctreeNode(lvl + 1, this, new Vect3d(minX, medY, medZ), new Vect3d(medX, maxY, maxZ), octree);
        children[4] = new OctreeNode(lvl + 1, this, new Vect3d(minX, medY, minZ), new Vect3d(medX, maxY, medZ), octree);
        children[5] = new OctreeNode(lvl + 1, this, new Vect3d(medX, medY, minZ), new Vect3d(maxX, maxY, medZ), octree);
        children[7] = new OctreeNode(lvl + 1, this, new Vect3d(medX, medY, medZ), new Vect3d(maxX, maxY, maxZ), octree);
        children[2] = new OctreeNode(lvl + 1, this, new Vect3d(minX, minY, medZ), new Vect3d(medX, medY, maxZ), octree);
        children[0] = new OctreeNode(lvl + 1, this, new Vect3d(minX, minY, minZ), new Vect3d(medX, medY, medZ), octree);
        children[1] = new OctreeNode(lvl + 1, this, new Vect3d(medX, minY, minZ), new Vect3d(maxX, medY, medZ), octree);
        children[3] = new OctreeNode(lvl + 1, this, new Vect3d(medX, minY, medZ), new Vect3d(maxX, medY, maxZ), octree);
        
        for(Triangle3d triangle : contents)
        {
            insert(triangle);
        }
        
        contents.clear();
    }

    public OctreeNode(int lvl, OctreeNode parent, Vect3d min, Vect3d max, Octree tree) {
        color = NodeColor.WHITE;
        this.lvl = lvl;
        this.octree = tree;
        this.parent = parent;

        children = new OctreeNode[8];
        for (int i = 0; i < 8; ++i) {
            children[i] = null;
        }

        this.min = min;
        this.max = max;

        contents = new ArrayList<Triangle3d>();
    }

    public void insert(Triangle3d t) {
        if (lvl == octree.getMaxLvl() || (contents.size() < octree.getMaxTriPerNode() && !hasChildren()) ) {
            contents.add(t);
        } else {
            if (!hasChildren()) {
                initChildren();
            }

            if (t.tri_AABB(children[0].getAABB())) {
                children[0].insert(t);
            }
            if (t.tri_AABB(children[1].getAABB())) {
                children[1].insert(t);
            }
            if (t.tri_AABB(children[2].getAABB())) {
                children[2].insert(t);
            }
            if (t.tri_AABB(children[3].getAABB())) {
                children[3].insert(t);
            }
            if (t.tri_AABB(children[4].getAABB())) {
                children[4].insert(t);
            }
            if (t.tri_AABB(children[5].getAABB())) {
                children[5].insert(t);
            }
            if (t.tri_AABB(children[6].getAABB())) {
                children[6].insert(t);
            }
            if (t.tri_AABB(children[7].getAABB())) {
                children[7].insert(t);
            }
        }
    }

    public boolean hasChildren() {
        return (children[0] != null);
    }

    public AABB getAABB() {
        return new AABB(min, max);
    }

    public OctreeNode[] getChildren() {
        return children;
    }

    public void clasifyColor(TriangleMesh t) {

        if (!contents.isEmpty()) {
            color = NodeColor.GREY;
        } else {
            Vect3d v = new Vect3d(min);
            v.add(max.sub(min));
            if (t.pointIntoMesh(v)) {
                color = NodeColor.BLACK;
            } else {
                color = NodeColor.WHITE;
            }
        }
        if (hasChildren()) {
            for (OctreeNode nodo : getChildren()) {
                nodo.clasifyColor(t);
            }
        }
    }

}
