package algGeom.lib3D;

import Util.*;
import static java.lang.Math.abs;

enum intersectionType {
    POINT, SEGMENT, COPLANAR
}

public class Plane {

    static public class IntersectionTriangle {

        public intersectionType type;
        public Vect3d p;
        public Segment3d s;
    }

    static public class IntersectionLine {

        public intersectionType type;
        public Vect3d p;
    }

    Vect3d a, b, c; //tres puntos cualquiera del plano  

    /**
     *
     * @param p en pi = p+u * lambda + v * mu -> r en los puntos (R,S,T)
     * @param u en pi = p+u * lambda + v * mu -> d en los puntos (R,S,T)
     * @param v en pi = p+u * lambda + v * mu -> t en los puntos (R,S,T)
     * @param arePoints = false, then params are p+u * lambda + v * mu,
     * otherwise are points (R,S,T)
     */
    public Plane(Vect3d p, Vect3d u, Vect3d v, boolean arePoints) {
        if (!arePoints) { //are vectors: pi =  p+u * lambda + v * mu 
            a = p;
            b = u.add(a);
            c = v.add(a);
        } else { // are 3 points in the plane 
            a = p;
            b = u;
            c = v;
        }
    }

    /**
     *
     * @return A in AX+BY+CZ+D = 0;
     *
     */
    public double getA() {

        return (BasicGeom.determinant2x2(c.getZ() - a.getZ(), c.getY() - a.getY(), b.getY() - a.getY(), b.getZ() - a.getZ()));
    }

    /**
     *
     * @return B in AX+BY+CZ+D = 0;
     *
     */
    public double getB() {

        return (BasicGeom.determinant2x2(c.getX() - a.getX(), c.getZ() - a.getZ(), b.getZ() - a.getZ(), b.getX() - a.getX()));
    }

    /**
     *
     * @return C in AX+BY+CZ+D = 0;
     *
     */
    public double getC() {
        return (BasicGeom.determinant2x2(c.getY() - a.getY(), c.getX() - a.getX(), b.getX() - a.getX(), b.getY() - a.getY()));
    }

    /**
     *
     * @return D in AX+BY+CZ+D = 0;
     *
     */
    public double getD() {

        return (-1) * (getA() * a.getX() + getB() * a.getY() + getC() * a.getZ());
    }

    /**
     *
     * @return the normal vector of (A,B,C) in Ax+By+Cz+D = 0
     */
    public Vect3d getNormal() {
        double A = getA();
        double B = getB();
        double C = getC();
        Vect3d normal = new Vect3d(A, B, C);
        return normal.scalarMul(1 / normal.module());
    }

    /**
     * @return the point of the parametric function (plano=p+u*lambda+v*mu)
     */
    public Vect3d getPointParametric(double lambda, double mu) {
        Vect3d u = b.sub(a),
                v = c.sub(a);

        return a.add(u.scalarMul(lambda)).add(v.scalarMul(mu));
    }

    /**
     * @return Distance between a plane/point
     */
    public double distance(Vect3d p) {
        double A = getA();
        double B = getB();
        double C = getC();
        double D = getD();

        return Math.abs(A * p.x + B * p.y + C * p.z + D) / Math.sqrt(A * A + B * B + C * C);
    }

    /**
     * @return true if p is in the plane
     */
    public boolean coplanar(Vect3d p) {
        if (distance(p) < BasicGeom.ZERO) {
            return true;
        }
        return false;
    }

    public Line3d intersect(Plane p) {
        Vect3d n = getNormal().xProduct(p.getNormal());

        Double A1 = getA(), A2 = p.getA(), A3 = n.x;
        Double B1 = getB(), B2 = p.getB(), B3 = n.y;
        Double C1 = getC(), C2 = p.getC(), C3 = n.z;
        Double D1 = getD(), D2 = p.getD();

        double DET = BasicGeom.determinant3x3(A1, B1, C1, A2, B2, C2, n.x, n.y, n.y);

        if (DET < BasicGeom.ZERO) {
            double x = D2 * BasicGeom.determinant2x2(B1, C1, B3, C3)
                    + D1 * BasicGeom.determinant2x2(B2, C2, B3, C3);
            double y = D2 * BasicGeom.determinant2x2(A3, C3, A1, C1)
                    + D1 * BasicGeom.determinant2x2(A3, C3, A2, C2);
            double z = D2 * BasicGeom.determinant2x2(A1, B1, A3, B3)
                    + D1 * BasicGeom.determinant2x2(A2, B2, A3, B3);
            x /= DET;
            y /= DET;
            z /= DET;

            return new Line3d(new Vect3d(x, y, z), new Vect3d(x + A3, y + B3, z + C3));
        }

        return null;
    }

    public Vect3d intersect(Line3d p) {
        Vect3d n = getNormal();
        double D = getD();

        Vect3d v = p.dest.sub(p.orig);

        if (!BasicGeom.equal(n.dot(v), 0)) {
            double lambda = (n.dot(p.orig) + D) * -1 / (n.dot(v) + D);
            return p.getPoint(lambda);
        }

        return null;
    }

    /**
     * shows the plane values
     */
    public void out() {
        System.out.print("Plane->a: ");
        System.out.println(a);
        System.out.print("Plane->b: ");
        System.out.println(b);
        System.out.print("Plane->c: ");
        System.out.println(c);
    }

}
